<?php
// Require the bundled autoload file - the path may need to change
// based on where you downloaded and unzipped the SDK
require __DIR__ . '/twilio-php-master/Twilio/autoload.php';

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

// Your Account SID and Auth Token from twilio.com/console
$sid = 'AC2445657d0ac6ecb731713a41fc3d19a9';
$token = 'f234aee8521e4c8c12c64ef99529c38d';

$client = new Twilio\Rest\Client($sid, $token);

$message = $client->account->messages->create(
  '+A5522998291722', // Text this number
  array(
    'from' => '+17028672747', // From a valid Twilio number
    'body' => 'Enviando uma mensagem! Link https://demo.twilio.com/owl.png'
  )
);

echo '<pre>';
print $message->status;