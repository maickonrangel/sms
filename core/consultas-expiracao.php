<?php

$expirado_sim 				= $conn->query("SELECT COUNT(id) AS expirado_sim FROM pesquisa_satisfacao WHERE expirado = 1 {$ano}");
$resposta_expirado_sim		= $expirado_sim->fetchAll(PDO::FETCH_ASSOC);
$resposta_expirado_sim 		= $resposta_expirado_sim[0]['expirado_sim'];

$expirado_nao 				= $conn->query("SELECT COUNT(id) AS expirado_nao FROM pesquisa_satisfacao WHERE expirado = 0 {$ano}");
$resposta_expirado_nao		= $expirado_nao->fetchAll(PDO::FETCH_ASSOC);
$resposta_expirado_nao 		= $resposta_expirado_nao[0]['expirado_nao'];
