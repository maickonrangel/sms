<?php 
	$consulta = $conn->query("SELECT COUNT(id) AS total_pesquisado FROM pesquisa_satisfacao");
	$total_pesquisado = $consulta->fetch(PDO::FETCH_ASSOC);
	
	$consulta_count_ruim 	= $conn->query("SELECT COUNT(id) AS total_ruim FROM pesquisa_satisfacao WHERE resposta1 = 'ruim'");
	$resposta_ruim 			= $consulta_count_ruim->fetch(PDO::FETCH_ASSOC);

	$consulta_count_regular = $conn->query("SELECT COUNT(id) AS total_regular FROM pesquisa_satisfacao WHERE resposta1 = 'regular'");
	$resposta_regular 		= $consulta_count_regular->fetch(PDO::FETCH_ASSOC);

	$consulta_count_bom 	= $conn->query("SELECT COUNT(id) AS total_bom FROM pesquisa_satisfacao WHERE resposta1 = 'bom'");
	$resposta_bom 			= $consulta_count_bom->fetch(PDO::FETCH_ASSOC);

	$consulta_count_otimo 	= $conn->query("SELECT COUNT(id) AS total_otimo FROM pesquisa_satisfacao WHERE resposta1 = 'otimo'");
	$resposta_otimo 		= $consulta_count_otimo->fetch(PDO::FETCH_ASSOC);


	$consulta_ruim 					= $conn->query("SELECT * FROM pesquisa_satisfacao WHERE resposta1 = 'ruim'");
	$resposta_usuarios_ruim			= $consulta_ruim->fetchAll(PDO::FETCH_ASSOC);

	$consulta_regular 				= $conn->query("SELECT * FROM pesquisa_satisfacao WHERE resposta1 = 'regular'");
	$resposta_usuarios_regular 		= $consulta_regular->fetchAll(PDO::FETCH_ASSOC);

	$consulta_bom 					= $conn->query("SELECT * FROM pesquisa_satisfacao WHERE resposta1 = 'bom'");
	$resposta_usuarios_bom 			= $consulta_bom->fetchAll(PDO::FETCH_ASSOC);

	$consulta_otimo 				= $conn->query("SELECT * FROM pesquisa_satisfacao WHERE resposta1 = 'otimo'");
	$resposta_usuarios_otimo 		= $consulta_otimo->fetchAll(PDO::FETCH_ASSOC);
	
	$consulta_geral 				= $conn->query("SELECT * FROM pesquisa_satisfacao");
	$resposta_usuarios_geral 		= $consulta_geral->fetchAll(PDO::FETCH_ASSOC);

	$total_pesquisado 	= $total_pesquisado['total_pesquisado'];
	$resposta_ruim 		= $resposta_ruim['total_ruim'];
	$resposta_regular 	= $resposta_regular['total_regular'];
	$resposta_bom 		= $resposta_bom['total_bom'];
	$resposta_otimo 	= $resposta_otimo['total_otimo'];

	$porcentagem_ruim 		= ($resposta_ruim * 100) / $total_pesquisado;
	$porcentagem_regular 	= ($resposta_regular * 100) / $total_pesquisado;
	$porcentagem_bom 		= ($resposta_bom * 100) / $total_pesquisado;
	$porcentagem_otimo 		= ($resposta_otimo * 100) / $total_pesquisado;

	$porcentagem_ruim 		= number_format($porcentagem_ruim, 2, '.', '');
	$porcentagem_regular 	= number_format($porcentagem_regular, 2, '.', '');
	$porcentagem_bom 		= number_format($porcentagem_bom, 2, '.', '');
	$porcentagem_otimo 		= number_format($porcentagem_otimo, 2, '.', '');