<?php

// production
// define("URL_BASE", 'http://uniqsuporte.com.br/sms/');
// $host 	= "localhost";
// $dbname = "amindcom_sms_teste";
// $user 	= "amindcom_sms";
// $pass 	= "GS%$~f+n00.A";	 

// local
define("URL_BASE", 'http://127.0.0.1/sms/');
$host 	= "localhost";
$dbname = "sms_pool";
$user 	= "root";
$pass 	= "";

try {
	$conn = new PDO("mysql:host={$host};dbname={$dbname}", "{$user}", "{$pass}");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    echo 'ERROR: ' . $e->getMessage();
}
