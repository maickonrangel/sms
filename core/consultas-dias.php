<?php
$ultimos_dias = [];

if (is_array($_SESSION['dias'])) {
	foreach ($_SESSION['dias'] as $key => $value) {
		$pesquisa 			= $conn->query("SELECT count(id) as pesquisa FROM pesquisa_satisfacao WHERE data_preenchimento > (NOW() - INTERVAL {$value} DAY) {$ano}");
		$resposta_pesquisa	= $pesquisa->fetchAll(PDO::FETCH_ASSOC);
		$ultimos_dias[] = [
			'label' 	=> "{$value} dias",
			'y' 		=> intval($resposta_pesquisa[0]['pesquisa']),
			'x'			=> $key
		];
	}
} else {
	$pesquisa 			= $conn->query("SELECT count(id) as pesquisa FROM pesquisa_satisfacao WHERE data_preenchimento > (NOW() - INTERVAL {$_SESSION['dias']} DAY) {$ano}");
	$resposta_pesquisa	= $pesquisa->fetchAll(PDO::FETCH_ASSOC);
	$ultimos_dias[] = [
		'label' 	=> "{$_SESSION['dias']} dias",
		'y' 		=> intval($resposta_pesquisa[0]['pesquisa']),
		'x'			=> $key
	];
}


