<?php

$meses = [
	"Janeiro",
	"Fevereiro",
	"Março",
	"Abril",
	"Maio", 
	"Junho",
	"Julho",
	"Agosto",
	"Setembro",
	"Outubro",
	"Novembro",
	"Dezembro"
];

$grafico_meses = [];

if (is_array($_SESSION['meses']) && $_SESSION['meses'][0] != 'todos') {
	foreach ($_SESSION['meses'] as $key => $value) {
		$consulta 	= $conn->query("SELECT COUNT(id) AS total_do_mes  FROM `pesquisa_satisfacao` WHERE MONTH(data_preenchimento) = '{$value}' {$ano}");
		$mes 		= $consulta->fetchAll(PDO::FETCH_ASSOC);
		$grafico_meses[] = [
			'name' 	=> $meses[$value-1],
			'label' => $meses[$value-1],
			'y'		=> intval($mes[0]['total_do_mes'])
		];
	}
} else {
	if ($_SESSION['meses'][0] == 'todos') {
		for ($i=1; $i <= 12; $i++) { 
			$consulta 	= $conn->query("SELECT COUNT(id) AS total_do_mes  FROM `pesquisa_satisfacao` WHERE MONTH(data_preenchimento) = '{$i}' {$ano}");
			$mes 		= $consulta->fetchAll(PDO::FETCH_ASSOC);
			$grafico_meses[] = [
				'name' 	=> $meses[$i-1],
				'label' => $meses[$i-1],
				'y'		=> intval($mes[0]['total_do_mes'])
			];
		}
	} else {
		$consulta 	= $conn->query("SELECT COUNT(id) AS total_do_mes  FROM `pesquisa_satisfacao` WHERE MONTH(data_preenchimento) = '{$_SESSION['meses'][0]}' {$ano}");
		$mes 		= $consulta->fetchAll(PDO::FETCH_ASSOC);
		$grafico_meses[] = [
			'name' 	=> $meses[$_SESSION['meses']-1],
			'label' => $meses[$_SESSION['meses']-1],
			'y'		=> intval($mes[0]['total_do_mes'])
		];
	}
}
