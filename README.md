# TalkToMe

Projeto de pesquisa de satisfação para análise de mercado.

# Instalação

Faça o download do [XAMPP](https://www.apachefriends.org/pt_br/download.html)

Após a instalação do XAMPP acesse pasta C:/xampp/htdocs/ e crie uma nova pasta chamada __sms__

Dentro da pasta __sms__ copie e cole todos os arquivos do projeto.

### Acessando localmente

Clique no executável do __XAMPP__ e após parecer verde no painel dele, acesse o endereço <http://127.0.0.1/phpmyadmin/index.php>
para fazer a criação do banco de dados.

Dentro deste endereço clique no botão __importar__, depois em __escolher arquivo__ e selecione o arquivo __sms_db.sql__ que se encontra na pasta raiz do projeto.

Depois clique em __executar__ para importar o banco de dados do sistema.

Feito isso, entre na URL <http://127.0.0.1/sms/dashboard> e faça login com os seguintes dados:
<br>
__usuário:__maickon4developers@gmail.com
__senha:__maickon123
