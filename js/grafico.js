window.onload = function () {

var grafico_geral = {
    title: {
        text: 'Atendimento',
        fontFamily: 'arial'         
    },
    subtitles:[
        {
            text: "O atendimento foi bom?",
            fontSize: 20,
            fontFamily: 'arial',
            fontWeight: 'normal'
        }
    ],
    data: [              
    {
        type: "bar",
            dataPoints: [
              { label: "Ótimo",  y: RESPOSTA_OTIMO, color: "rgb(50,205,50, 0.9)"  },
              { label: "Bom", y: RESPOSTA_BOM , color: "rgb(34,139,34, 0.9)" },
              { label: "Regular", y: RESPOSTA_REGULAR, color: "rgb(255,255,0, 0.9)"  },
              { label: "Ruim",  y: RESPOSTA_RUIM, color: "rgb(255,0,0, 0.9)"  }
            ]
        }
    ]
};

var grafico_meses = {
    theme: "light2",    // "light1", "dark1", "dark2"
    title: {
        text: 'Questionários responditos',
        fontFamily: 'arial',
        fontWeight: 'normal'
    },
    subtitles:[
        {
            text: "Ao longo de cada mês",
            fontSize: 20,
            fontFamily: 'arial',
            fontWeight: 'normal'
        }
    ],
    axisY: {
        title: "Respostas"
    },
    axisX: {
        title: "Meses"
    },
    legend: {
        maxWidth: 300
    },
    data: [              
        {
            cursor: "pointer",
            type: "pie",
            toolTipContent: "{label}: {y} Cadastros",
            showInLegend: true,
            dataPoints: MESES
        }
    ]
};


var grafico_tickets = {
    title: {
        text: 'Tickets usados',
        fontFamily: 'arial'         
    },
    subtitles:[
        {
            text: "Comparativo entre o uso dos tickets",
            fontSize: 20,
            fontFamily: 'arial',
            fontWeight: 'normal'
        }
    ],
    data: [              
        {
            type: "doughnut",
            showInLegend: true,
            indexLabelPlacement: "outside",
            toolTipContent: "{label}: {y} pessoas ",
            dataPoints: [
                { label: "Ticket Utilizado",  y: TICKETS_SIM, color: "rgb(255,255,0, 0.9)", legendText: "Ticket Utilizado"  },
                { label: "Ticket não Utilizado", y: TICKETS_NAO , color: "rgb(255,0,0, 0.9)", legendText: "Ticket não Utilizado" }
            ]
        }
    ]
};

var grafico_ultimos_dias = {
    title: {
        text: 'Questionários respondidos',
        fontFamily: 'arial'         
    },
    subtitles:[
        {
            text: "nos últimos dias",
            fontSize: 20,
            fontFamily: 'arial',
            fontWeight: 'normal'
        }
    ],
    axisY: {
        title: "Quantidade de respostas"
    },
    axisX: {
        title: "Dias decorridos"
    },
    data: [              
        {
            type: "column",
            toolTipContent: "Últimos {label}:  {y} Respostas ",
            dataPoints: ULTIMOS_DIAS
        }
    ]
};

$("#grafico-geral").CanvasJSChart(grafico_geral);
$("#grafico-meses").CanvasJSChart(grafico_meses);
$("#grafico-tickets").CanvasJSChart(grafico_tickets);
$("#grafico_ultimos_dias").CanvasJSChart(grafico_ultimos_dias);

}