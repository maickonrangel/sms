<?php use Twilio\Rest\Client; ?>
<!DOCTYPE html>
<html>
<head>
	<title>SMS Pesquisa</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="css/index.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

	<div class="container">
		<form action="" method="post">
			<div id="part-1">
				<div class="row">
					<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
						<h1 class="title">O que você achou do atendimento?</h1>
						<input type="hidden" name="enviar" value="enviar">
						<div class="col-md-3 col-xs-12 col-sm-3 col-lg-3">
						  <input type="radio" id="otimo" name="atendimento" value="otimo" />
						  <img class="radio" data-check="otimo" src="img/otimo.fw.png">
						  <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 label" data-check="otimo">Ótimo</div>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-3 col-lg-3">
						  <input type="radio" id="bom" checked="checked" name="atendimento" value="bom" />
						  <img class="radio" data-check="bom" src="img/bom.fw.png">
						  <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 label" data-check="bom">Bom</div>
						</div>
						<div class="col-md-3 col-xs-12 col-sm-3 col-lg-3">
						  <input type="radio" id="regular" name="atendimento" value="regular" />
						  <img class="radio" data-check="regular" src="img/regular.fw.png">
						  <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 label" data-check="regular">Regular</div>
						</div>
				  		<div class="col-md-3 col-xs-12 col-sm-3 col-lg-3">
						  <input type="radio" id="ruim" name="atendimento" value="ruim" />
						  <img class="radio" data-check="ruim" src="img/ruim.fw.png">
						  <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 label" data-check="ruim">Ruim</div>
						</div>
					</div>
				</div>
				<br>
			</div>
		
			<div id="part-2">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
							<!-- <img class="img-responsive obrigado" src="img/obrigado.png"> -->
							<h1 class="title">Obrigado</h1>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
							<h1 class="title">Informe seu número de telefone:</h1>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
						  	<input type="number" id="telefone" min="" max="99999999999" required="required" placeholder="Informe seu telefone..." name="telefone" value="22">
						</div>
					</div>
					<br>
					<br>
					<div class="row">
						<div class="col-md-12">
							<input type="submit" id="enviar" value="Enviar">
							<a id="voltar" href="">Voltar</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<script src=""></script>
	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="js/index.js"></script>
</body>
</html>

<?php

if (isset($_REQUEST) && isset($_REQUEST['enviar'])) {
	require __DIR__ . '/twilio-php-master/Twilio/autoload.php';
	require_once 'core/conexao.php';
	require_once 'core/helper.php';

	// Your Account SID and Auth Token from twilio.com/console
	$sid = 'AC2445657d0ac6ecb731713a41fc3d19a9';
	$token = 'f234aee8521e4c8c12c64ef99529c38d';

	$client = new Twilio\Rest\Client($sid, $token);
	
	$ticket = rstr(5);
	$data = new DateTime();
    $dataHora = $data->format('d-m-Y H:i:s');
   
	$sql = "INSERT INTO pesquisa_satisfacao (resposta1, telefone, ticket, expirado, data_preenchimento) 
	VALUES(:resposta1, :telefone, :ticket, :expirado, :data_preenchimento)";
	$stmt = $conn->prepare($sql);
	$stmt->execute(array(
	    ':resposta1' => $_REQUEST['atendimento'],
	    ':telefone' => $_REQUEST['telefone'],
	    ':ticket' => $ticket,
	    ':expirado' => 0,
	    ':data_preenchimento' => $dataHora
	));
	
	if ($stmt->rowCount() == 1) {
		$msg = "Obrigado pela participação.";
		$msg .= "Acesse: http://uniqsuporte.com.br/sms/ticket/usar.php e informe este código: ({$ticket}).";
		$msg .= "Ticket gerado em: {$dataHora}";
		$saida = $client->messages->create(
		  '+55' . $_REQUEST['telefone'], // Text this number
		  array(
		    'from' => '+17099090602', // From a valid Twilio number
		    'body' => $msg
		  )
		);

		if ($saida->status == 'queued') {
			header("location: ".URL_BASE."ticket");
		} else if ($saida->status == 'failed') {
			header("location: ".URL_BASE."ticket?status=erro&msg={$saida['mensagem']}");
		} else {
			header("location: ".URL_BASE."ticket?status=erro&msg={$saida['mensagem']}");
		}
	} else {
		header("location: ".URL_BASE."ticket?status=erro&msg=erro-de-banco-de-dados");
	}
}