<!DOCTYPE html>
<html>
<head>
	<title>SMS Pesquisa</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="../css/index.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	<div>
		<form method="post" action="">
			<div class="container">
				<?php
					require_once '../core/conexao.php';

					if (isset($_REQUEST['ticket'])) {
						$consulta = $conn->query("SELECT * FROM pesquisa_satisfacao WHERE ticket='{$_REQUEST['ticket']}'");
						$resposta = $consulta->fetch(PDO::FETCH_ASSOC);
						if (isset($resposta['id'])) {
							$stmt = $conn->prepare("UPDATE pesquisa_satisfacao SET expirado=:expirado WHERE ticket=:ticket");
							$stmt->execute(array(
								':expirado' => 1,
								':ticket' 	=> $_REQUEST['ticket']
							));
							 
							if ($stmt->rowCount() == 1) {
								echo '<div class="alert alert-success" role="alert" style="margin:0px;margin-top:10px;">
										<strong>Ticket usado com sucesso!</strong>
									</div>';
							} else {
								echo '
								<div class="alert alert-warning" role="alert" style="margin:0px;margin-top:10px;">
									<strong>Este ticket já foi utilizado!</strong>
								</div>';
							}
						} else {
							echo '
								<div class="alert alert-danger" role="alert" style="margin:0px;margin-top:10px;">
									<strong>Código do ticket Inválido!</strong>
								</div>';
						}
					}

					?>
				<div class="row">
					<div class="col-md-12">
						<h1 class="title">Insira o código do ticket promocional aqui:</h1>
					</div>
					<div class="col-md-12">
					  	<input id="ticket" required="required" placeholder="Informe o código do ticket aqui..." name="ticket">
					</div>
				</div>
				<br>
				<br>
				<div class="row">
					<div class="col-md-12">
						<input type="submit" id="enviar" value="Enviar">
						<a href="<?php echo URL_BASE ?>dashboard" id="voltar">Voltar</a>
					</div>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<script src="../js/jquery.maskedinput.min.js"></script>
	<script type="../text/javascript" src="js/index.js"></script>
</body>
</html>