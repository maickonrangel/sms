-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 26-Ago-2018 às 14:36
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sms_pool`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `pesquisa_satisfacao`
--

CREATE TABLE `pesquisa_satisfacao` (
  `id` int(11) NOT NULL,
  `resposta1` varchar(255) NOT NULL,
  `telefone` varchar(255) NOT NULL,
  `ticket` varchar(255) NOT NULL,
  `expirado` varchar(255) NOT NULL DEFAULT '0',
  `data_preenchimento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pesquisa_satisfacao`
--

INSERT INTO `pesquisa_satisfacao` (`id`, `resposta1`, `telefone`, `ticket`, `expirado`, `data_preenchimento`) VALUES
(2, 'ruim', '22 998291722', 'PU8ZH', '1', '2018-08-14 12:46:00'),
(3, 'regular', '22 998191732', 'AC18B', '1', '2018-12-21 20:48:49'),
(4, 'otimo', '22 998191040', 'ATK001', '1', '2018-08-02 11:00:28'),
(5, 'otimo', '22 998191732', 'AC18B', '1', '2018-04-21 13:05:10'),
(6, 'otimo', '22 998191040', 'ATK01', '1', '2018-08-21 16:45:16'),
(7, 'ruim', '22 998191732', 'AC18A', '1', '2018-01-21 12:49:56'),
(8, 'regular', '22 998191040', 'ATKC1', '1', '2018-03-21 13:19:20'),
(9, 'regular', '22 998191732', 'AC18B', '1', '2018-09-21 16:27:41'),
(10, 'regular', '22 998191040', 'ATK9C', '0', '2018-02-21 19:44:45'),
(11, 'bom', '22 998191732', 'AC1WB', '1', '2018-08-21 20:36:59'),
(12, 'bom', '22 998191040', 'ATW01', '0', '2018-02-21 21:11:45'),
(13, 'bom', '22 998191732', 'AR18B', '1', '2018-03-21 23:25:38'),
(14, 'ruim', '22 998191040', 'CDK01', '0', '2018-12-22 01:43:50'),
(15, 'bom', '22 998191732', 'AC0OB', '0', '2018-01-21 20:27:04'),
(16, 'regular', '22 998191040', 'ATR65', '1', '2018-12-21 21:53:09'),
(17, 'otimo', '22 998191732', 'AC190', '0', '2018-01-21 18:08:45'),
(18, 'ruim', '22 998191040', '9ATK1', '1', '2018-05-21 20:15:12'),
(19, 'ruim', '22 998191732', '1C18B', '0', '2018-03-21 16:59:22'),
(20, 'bom', '22 998191040', '2TJ01', '1', '2018-11-21 08:29:46'),
(21, 'bom', '22 998191732', 'AC2OB', '0', '2018-06-21 23:43:11'),
(22, 'regular', '22 998191040', 'BZK19', '1', '2018-10-21 09:49:30'),
(23, 'regular', '22 998191732', 'TC190', '0', '2018-01-21 16:43:56'),
(24, 'otimo', '22 998191040', 'TTTK1', '0', '2018-07-22 00:40:46'),
(25, 'otimo', '22 998291722', 'SW1LB', '0', '2018-11-22 01:09:52'),
(26, 'otimo', '22 998291722', 'GJZE8', '0', '2018-12-21 06:56:15'),
(27, 'otimo', '22 998291722', '39FSE', '0', '2018-05-21 09:01:18'),
(28, 'otimo', '22 998291722', 'R99H2', '0', '2018-06-21 15:56:17'),
(29, 'bom', '22 998291722', 'D5Q70', '0', '2018-08-21 06:41:12'),
(30, 'ruim', '22 998291722', 'L5TPP', '0', '2018-11-22 00:46:11'),
(31, 'bom', '22 998183022', 'EVLGN', '0', '2018-08-21 15:38:42'),
(32, 'otimo', '22 ', 'X5XQ0', '0', '2018-08-21 04:12:56'),
(33, 'regular', '(22) 9992-43957', 'FTXDB', '0', '2018-02-21 19:58:55'),
(34, 'bom', '(22) 98987-7721', 'B2QK7', '0', '2018-09-21 18:06:34'),
(35, 'otimo', '(22) 99829-1722', 'H1ERN', '0', '2018-07-21 06:52:04'),
(36, 'bom', '(22) 99829-1722', 'WVONJ', '0', '2018-08-21 04:07:32'),
(37, 'otimo', '(22) 90903-0309', 'HLKCU', '0', '2018-05-21 06:34:27'),
(38, 'otimo', '(22) 99924-3957', '76157', '0', '2018-07-21 13:20:29'),
(39, 'bom', '(22) 9992-43957', 'TRMME', '0', '2018-06-21 20:10:45'),
(40, 'ruim', '(22) 9992-43957', 'AS94V', '0', '2018-04-21 08:02:34'),
(41, 'ruim', '(22) 99829-1722', 'R0RB0', '0', '2018-10-21 08:37:30'),
(42, 'regular', '(22) 99924-3957', 'K5FXT', '0', '2018-08-21 23:59:34'),
(43, 'bom', '22998292722', 'ACCOB', '0', '2018-11-21 15:09:08'),
(44, 'bom', '22998291722', 'UXVXP', '0', '2018-03-21 15:54:05'),
(45, 'bom', '22', '6OLAH', '0', '2018-09-21 06:32:21'),
(46, 'otimo', '22998291722', 'AGXNB', '0', '2018-02-21 20:02:04'),
(47, 'regular', '22998291722', 'Z5H3X', '0', '2018-03-22 01:54:49'),
(48, 'regular', '22998291722', 'PYF2B', '0', '2018-05-21 10:16:31'),
(49, 'regular', '22998291722', 'XKHLC', '0', '2018-10-21 13:36:50'),
(50, 'regular', '22998291722', 'E0VPI', '0', '2018-05-21 16:26:32'),
(51, 'bom', '22999999999', '5X9TB', '0', '2018-05-21 09:01:23'),
(52, 'otimo', '22998291722', '3UVFL', '0', '2018-11-21 11:01:01'),
(53, 'otimo', '22998291722', '55FTJ', '0', '0000-00-00 00:00:00'),
(54, 'otimo', '22998291722', '9EBNI', '0', '0000-00-00 00:00:00'),
(55, 'otimo', '22999', 'F1TMV', '0', '0000-00-00 00:00:00'),
(56, 'otimo', '22999', 'HK3N9', '0', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `cadastrado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `senha`, `cadastrado`) VALUES
(1, 'admin@email.com', '$2y$10$t0O2BKDWTpAFAuL0P0dJLOwRnPHp8k5Fax5X//0IYs3NiYbDsy6uG', '2018-07-17 16:47:22'),
(2, 'maickon4developers@gmail.com', '$2y$10$knF1NUJqNvAhWe8Nq26Gh.KwpnXBVG1jvAEckwftCVps2CDhGB1Ni', '2018-07-17 16:47:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pesquisa_satisfacao`
--
ALTER TABLE `pesquisa_satisfacao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pesquisa_satisfacao`
--
ALTER TABLE `pesquisa_satisfacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
