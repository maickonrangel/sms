<div class="">
    <h1 class="page-header">Configuração de pesquisa</h1>
    <div class="row">
        <form action="../dashboard/configuracao/index.php" method="post">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="sel1">Últimos meses (Segure Ctrl para marcar mais de um item):</label>
                    <select multiple class="form-control input-lg" name="meses[]" id="meses" style="height:280px;">
                        <option selected value="todos">Todos</option>
                        <option value="1">Janeiro</option>
                        <option value="2">Fevereiro</option>
                        <option value="3">Março</option>
                        <option value="4">Abril</option>
                        <option value="5">Maio</option>
                        <option value="6">Junho</option>
                        <option value="7">Julho</option>
                        <option value="8">Agosto</option>
                        <option value="9">Setembro</option>
                        <option value="10">Outubro</option>
                        <option value="11">Novembro</option>
                        <option value="12">Dezembro</option>
                    </select>
                </div>
            </div>
                
            <div class="col-md-6">
                <div class="form-group">
                    <label for="sel1">Últimos dias (Segure Ctrl para marcar mais de um item):</label>
                    <select multiple class="form-control input-lg" name="dias[]" id="dias" style="height:280px;">
                        <option selected value="10">10 dias</option>
                        <option selected value="20">20 dias</option>
                        <option selected value="30">30 dias</option>
                        <option value="60">40 dias</option>
                        <option value="90">50 dias</option>
                        <option value="120">60 dias</option>
                        <option value="120">70 dias</option>
                        <option value="120">80 dias</option>
                        <option value="120">90 dias</option>
                        <option value="120">100 dias</option>
                        <option value="120">110 dias</option>
                        <option value="120">120 dias</option>
                        <option value="120">130 dias</option>
                    </select>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label for="sel1">Selecione um ano:</label>
                    <select class="form-control input-lg" name="ano" id="anos">
                        <option value="todos">Todo o período</option>
                        <option value="2016">2016</option>
                        <option value="2017">2017</option>
                        <option selected value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                    </select>
                </div>
              <button type="submit" class="btn btn-default">Salvar configuração</button>
            </div>
        </form>
    </div>
</div>