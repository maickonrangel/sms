<h2 class="sub-header">Respostas</h2>
<?php 
    $data = [
        'ruim'          =>  $resposta_usuarios_ruim,
        'regular'       =>  $resposta_usuarios_regular,
        'bom'           =>  $resposta_usuarios_bom,
        'otimo'         =>  $resposta_usuarios_otimo,
        'geral'         =>  $resposta_usuarios_geral
    ];
    if (isset($_REQUEST['usuarios']) && isset($data[$_REQUEST['usuarios']])) {
        $dataBase = $data[$_REQUEST['usuarios']];
    } else {
        $dataBase = $data['geral'];
    }
    ?>
<div class="table-responsive">
    <table id="myTable" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>#id</th>
                <th>Resposta</th>
                <th>Telefone</th>
                <th>Ticket</th>
                <th>Ticket usado?</th>
                <th>Data cadastro</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dataBase as $key => $value): ?>
                <tr>
                    <td><?php echo $value['id'] ?></td>
                    <td class="<?php echo $value['resposta1'] ?>"><?php echo $value['resposta1'] ?></td>
                    <td><?php echo $value['telefone'] ?></td>
                    <td><?php echo $value['ticket'] ?></td>
                    <td><?php echo ($value['expirado']==1)?'sim':'não' ?></td>
                    <td><?php echo date('d/m/Y H:m:s', strtotime($value['data_preenchimento'])) ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>