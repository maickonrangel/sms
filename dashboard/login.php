<?php require_once '../core/conexao.php'; ?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* Full-width input fields */
input[type=email], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

/* Set a style for all buttons */
button {
    background-color: #FCBE1B;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}

/* Center the image and position the close button */
.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
    position: relative;
}

img.avatar {
    margin: 0 auto;
    width: 90%;
}

.container {
    padding: 16px;
}

.container label b{
    color: #FFF;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    padding-top: 60px;
}

/* Modal Content/Box */
.modal-content {
    text-align: center;
    margin: 0 auto;
    background-color: #0D4170;
    border: 1px solid #888;
    max-width: 500px; /* Could be more or less, depending on screen size */
}

.modal-content h2{
    color: #FFF;
}

.form{
    margin: auto;
}
/* Add Zoom Animation */
.animate {
    -webkit-animation: animatezoom 0.6s;
    animation: animatezoom 0.6s
}

@-webkit-keyframes animatezoom {
    from {-webkit-transform: scale(0)} 
    to {-webkit-transform: scale(1)}
}
    
@keyframes animatezoom {
    from {transform: scale(0)} 
    to {transform: scale(1)}
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}

.erro{
    background-color: rgb(255,255,255,0.4);
    padding: 5px;
    margin: 15px;
}
</style>
</head>
    <body style="overflow: auto!important;">

    <div class="form">
        <form class="modal-content animate" action="login.php" method="post">
            <h2>Login Dashboard</h2>
            <?php if (isset($_REQUEST['msg']) && $_REQUEST['msg'] == 'e'): ?>
                <div class="erro">
                    <p>Erro ao fazer login!</p>
                </div>
            <?php endif ?>
            <?php if (isset($_REQUEST['msg']) && $_REQUEST['msg'] == 'p'): ?>
                <div class="erro">
                    <p>Acesso restrito!</p>
                </div>
            <?php endif ?>
            <div class="imgcontainer">
                <img src="../img/logo.png" alt="Avatar" class="avatar">
            </div>

            <div class="container">
                <label for="uname"><b>Email</b></label>
                <input type="email" placeholder="Enter Username" name="email" required>

                <label for="psw"><b>Senha</b></label>
                <input type="password" placeholder="Enter Password" name="senha" required>
                
                <button type="submit">Entrar</button>
            </div>
        </form>
    </div>
    </body>
</html>

<?php

if (isset($_REQUEST['email']) && isset($_REQUEST['senha'])) {
    $sql = "SELECT * FROM usuarios";
    $select = $conn->query($sql);
    $result = $select->fetchAll(PDO::FETCH_OBJ);
    $logado = 0;
    foreach ($result as $key => $value) {
        if ($_REQUEST['email'] == $value->email && password_verify($_REQUEST['senha'], $value->senha)) {
            $logado = 1;
            break;
        } else {
            $logado = 0;
        }
    }

    if ($logado) {
        session_start();
        $_SESSION['emamil'] = $_REQUEST['email'];
        $_SESSION['status'] = 'login-ativo';
        $_SESSION['meses']  = [0 => 'todos'];
        $_SESSION['dias']   = [10, 20, 30];
        $_SESSION['ano']    = 2018;
        header("Location: ".URL_BASE."dashboard");
    } else {
        header("Location: ".URL_BASE."dashboard/login.php?msg=e");
    }
}