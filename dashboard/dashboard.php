<h1 class="page-header">Pesquisa de Satisfação</h1>
<div class="row placeholders">
   <div class="col-md-4 block">
        <p class="fa-2x">Total respondido <i class="fa fa-user-circle"></i></p>
        <div class="circle">
            <p><n class="fa-2x"><?php echo $total_pesquisado ?></n> <i class="fa fa-user-o fa-2x"></i></p>
        </div>
        <p><n class="fa-2x"><?php echo $total_pesquisado ?></n> respostas</p>
    </div>
    <div class="col-md-4 block">
        <p class="fa-2x">Atendimento <img src="../img/ruim.fw.png"></p>
        <div class="circle">
            <p><n class="fa-2x"><?php echo $porcentagem_ruim ?></n> <i class="fa fa-percent fa-2x"></i></p>
        </div>
        <p><n class="fa-2x"><?php echo $resposta_ruim ?></n> <i class="fa fa-user-circle fa-2x"></i> responderam</p>
    </div>
    <div class="col-md-4 block">
        <p class="fa-2x">Atendimento <img src="../img/regular.fw.png"></p>
        <div class="circle">
            <p><n class="fa-2x"><?php echo $porcentagem_regular ?></n> <i class="fa fa-percent fa-2x"></i></p>
        </div>
        <p><n class="fa-2x"><?php echo $resposta_regular ?></n> <i class="fa fa-user-circle fa-2x"></i> responderam</p>
    </div>
    <div class="col-md-4 block">
        <p class="fa-2x">Atendimento <img src="../img/bom.fw.png"></p>
        <div class="circle">
            <p><n class="fa-2x"><?php echo $porcentagem_bom ?></n> <i class="fa fa-percent fa-2x"></i></p>
        </div>
        <p><n class="fa-2x"><?php echo $resposta_bom ?></n> <i class="fa fa-user-circle fa-2x"></i> responderam</p>
    </div>
    <div class="col-md-4 block">
        <p class="fa-2x">Atendimento <img src="../img/otimo.fw.png"></p>
        <div class="circle">
            <p><n class="fa-2x"><?php echo $porcentagem_otimo ?></n> <i class="fa fa-percent fa-2x"></i></p>
        </div>
        <p><n class="fa-2x"><?php echo $resposta_otimo ?></n> <i class="fa fa-user-circle fa-2x"></i> responderam</p>
    </div>
</div>