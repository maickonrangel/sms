<?php
require_once '../core/conexao.php';
session_start();
if (!isset($_SESSION['status'])) {
    header("Location: ".URL_BASE."dashboard/login.php?msg=p");
}

if ($_SESSION['ano'] == 'todos') {
    $ano = '';
} else {
    $ano = "AND YEAR(data_preenchimento) = '{$_SESSION['ano']}'";
}

require_once '../core/consultas.php';
require_once '../core/consultas-mes.php';
require_once '../core/consultas-expiracao.php';
require_once '../core/consultas-dias.php';

?>
<!DOCTYPE html>
<html lang="pt-br">
    <?php require_once 'partials/header.php'; ?>
    <body>
        <?php require_once 'partials/menu.php'; ?>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <?php 
                    if (!isset($_REQUEST['pagina'])): 
                        require_once 'graficos/grafico-geral.php'; 
                        require_once 'graficos/grafico-tickets.php'; 
                        require_once 'graficos/grafico-meses.php'; 
                        require_once 'graficos/grafico-ultimos-dias.php'; 
                    else:
                        $paginas = ['login','tabelas','configuracao','dashboard'];
                        if (in_array($_REQUEST['pagina'], $paginas)) {
                            require_once $_REQUEST['pagina'].'.php'; 
                        } else {
                            require_once 'graficos/grafico-geral.php'; 
                            require_once 'graficos/grafico-tickets.php'; 
                            require_once 'graficos/grafico-meses.php'; 
                            require_once 'graficos/grafico-ultimos-dias.php'; 
                        }
                    endif 
                    ?>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="../js/canvas.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/dataTables.js"></script>
        <script type="text/javascript" src="https://getbootstrap.com/docs/3.3/dist/js/bootstrap.min.js"></script>
        <script src="../js/grafico.js"></script>
    </body>
</html>